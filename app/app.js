'use strict';

var app=angular.module('myApp',[]);

app.controller('inspectionController', function ($scope, $http) {
$scope.choiceSet = {choices: []};
    $scope.quest = {};

    $scope.choiceSet.choices = [];
    $scope.addNewChoice = function () {
        $scope.choiceSet.choices.push('');
    };

    $scope.removeChoice = function (z) {
        var lastItem = $scope.choiceSet.choices.length - 1;
        $scope.choiceSet.choices.splice(z,1);
    };
    });
